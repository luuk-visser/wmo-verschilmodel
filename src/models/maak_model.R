"
Dit script bevat functies voor het bouwen van diverse modellen alsook het 
visualiseren van de prestatie van deze modellen.
"



vng_model <- function(df, categorie, regionaalniveau, eigen_gemeente = "Amsterdam") {
  "
  Dit model bestaat uit een general linear model regressie met interacties.
  De keuze van de indicatoren is zowel gebaseerd op inhoudelijke argumenten als
  ook gemotiveerd vanuit de data.
  
  Het gaat om de volgende zet indicatoren:
  
  Op wijkniveau:
  
    Voor Hulpmiddelen en diensten (wijkniveau):
    diff_percentage_wmo_hd ~ -1 + stedelijkheid:diff_aandeel_65Plus + diff_aandeel_inBezitWoningcorporatie
    
    Voor ondersteuning thuis (wijkniveau):
    diff_percentage_wmo_oh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_N05_Psycholeptica + diff_N06_Psychoanaleptica  + ses_woa + diff_aandeel_GGZ_gebruikers  + diff_aandeel.mannen.en.vrouwen_75Plus
    
    Voor Huishouden (wijkniveau):
    diff_percentage_wmo_hh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa + diff_aandeel_ww + diff_C_Hartvaatstelsel + diff_M_Skeletspierstelsel + diff_N02_Analgetica + diff_aandeel_hoogopgeleiden + diff_aandeel.mannen.en.vrouwen_75Plus:stedelijkheid
  
    Voor Totaal (wijkniveau):
    diff_percentage_wmo_totaal ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa 

  Op gemeenteniveau (categorieën en subcategorieën):
  
    Voor Totaal (gemeenteniveau):
    diff_percentage_wmo_totaal ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa 
  
    Voor Huishouden (gemeenteniveau):
    diff_percentage_wmo_hh ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa + diff_aandeel_ww + diff_C_Hartvaatstelsel + diff_M_Skeletspierstelsel + diff_N02_Analgetica + diff_aandeel_hoogopgeleiden + diff_aandeel.mannen.en.vrouwen_75Plus:stedelijkheid
  
    Voor ondersteuning thuis (gemeenteniveau):
    diff_percentage_wmo_oh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_GGZ_gebruikers

    Voor Hulpmiddelen en diensten (gemeenteniveau):
    diff_percentage_wmo_hd ~ -1 + stedelijkheid:diff_aandeel_65Plus + diff_aandeel_inBezitWoningcorporatie
  
    Voor Woonvoorzieningen (gemeenteniveau):
    diff_percentage_wmo_sub_Woonvoorzieningen ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + diff_aandeel_huurwoningen + stedelijkheid
    
    Voor Vervoersvoorzieningen (gemeenteniveau):
    diff_percentage_wmo_sub_Vervoervoorzieningen ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + stedelijkheid + diff_C_Hartvaatstelsel
    
    Voor Rolstoelen (gemeenteniveau):
    diff_percentage_wmo_sub_Rolstoelen ~ -1 + stedelijkheid + diff_aandeel_65Plus 
  
    Voor Vervoersdiensten (gemeenteniveau):
    diff_percentage_wmo_sub_Vervoersdiensten ~ -1 + diff_aandeel_weduwen + diff_aantal_haltes_per_m2 + diff_aandeel.mannen.en.vrouwen_75Plus:afstandtotziekenhuis_sqrt

  
  parameters:
    df: data frame met verschillen
    categorie: de categorie die wordt voorspeld
    regionaalniveau: het regionale niveau waarop het model wordt gebouwd, keuze
      uit 'wijk' en 'gemeente'
    
  returns:
    lijst met het model en de gebruikte variabelen
  
  "
  print(sprintf("Grootte dataset voor model: %d", nrow(df)))

  if (regionaalniveau == "wijk") {
    formula <- case_when(
      categorie %like% "wmo_hd" ~ "diff_percentage_wmo_hd ~ stedelijkheid:diff_aandeel_65Plus + diff_aandeel_inBezitWoningcorporatie - 1",
      categorie %like% "wmo_oh" ~ "diff_percentage_wmo_oh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_N05_Psycholeptica + diff_N06_Psychoanaleptica  + ses_woa + diff_aandeel_GGZ_gebruikers  + diff_aandeel.mannen.en.vrouwen_75Plus",
      categorie %like% "wmo_hh" ~ "diff_percentage_wmo_hh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa + diff_aandeel_ww + diff_C_Hartvaatstelsel + diff_M_Skeletspierstelsel + diff_N02_Analgetica + diff_aandeel_hoogopgeleiden + diff_aandeel.mannen.en.vrouwen_75Plus:stedelijkheid",
      categorie %like% "wmo_totaal" ~ "diff_percentage_wmo_totaal ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa"
    )
  } else if (regionaalniveau == "gemeente") {
    formula <- case_when(
      categorie %like% "wmo_hd" ~ "diff_percentage_wmo_hd ~ -1 + stedelijkheid:diff_aandeel_65Plus + diff_aandeel_inBezitWoningcorporatie",
      categorie %like% "wmo_oh" ~ "diff_percentage_wmo_oh ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_GGZ_gebruikers",
      categorie %like% "wmo_hh" ~ "diff_percentage_wmo_hh ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa + diff_aandeel_ww + diff_C_Hartvaatstelsel + diff_M_Skeletspierstelsel + diff_N02_Analgetica + diff_aandeel_hoogopgeleiden + diff_aandeel.mannen.en.vrouwen_75Plus:stedelijkheid",
      categorie %like% "wmo_totaal" ~ "diff_percentage_wmo_totaal ~ -1 + diff_aandeel_65Plus + diff_aandeel_huishoudensZonderKinderen + diff_aandeel_ao + stedelijkheid + ses_woa",
      categorie %like% "wmo_sub_Woonvoorzieningen" ~ "diff_percentage_wmo_sub_Woonvoorzieningen ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + diff_aandeel_huurwoningen + stedelijkheid",
      categorie %like% "wmo_sub_Vervoervoorzieningen" ~ "diff_percentage_wmo_sub_Vervoervoorzieningen ~ -1 + diff_aandeel.mannen.en.vrouwen_75Plus + stedelijkheid + diff_C_Hartvaatstelsel",
      categorie %like% "wmo_sub_Rolstoelen" ~ "diff_percentage_wmo_sub_Rolstoelen ~ -1 + stedelijkheid + diff_aandeel_65Plus",
      categorie %like% "wmo_sub_Vervoersdiensten" ~ "diff_percentage_wmo_sub_Vervoersdiensten ~ -1 + diff_aandeel_weduwen + diff_aantal_haltes_per_m2 + diff_aandeel.mannen.en.vrouwen_75Plus:afstandtotziekenhuis_sqrt"
    )
  } else {
    stop("verkeerd regionaal niveau, kies uit c('wijk', 'gemeente')")
  }
  if(is.na(formula)){stop('verkeerde categorie')}
  
  # maak er een formule van
  formula <- as.formula(formula)
  print(formula)
  
  # filter op het juiste regionale niveau
  df <- filter(df, regionaalniveau==!!regionaalniveau)
  
  # verwijder alle data met na
  df <- df[complete.cases(df[,intersect(all.names(formula), names(df))]),]
  
  print('model wordt getraind op:')
  print_aantal_wijken(df)
  
  # cross-validatie
  # verdeel de dataset in 10 gelijke subsets
  folds <- cut(seq(1, nrow(df)), breaks=10, labels=FALSE)
  
  # Voer 10-voudige cross-validatie uit
  pred <- c()
  for(i in 1:10){
    #Segmenteer de data met de folds gebruik makend van de which() functie
    testIndexes <- which(folds==i, arr.ind=TRUE)
    testData <- df[testIndexes, ]
    trainData <- df[-testIndexes, ]
    mod <- glm(formula=formula, data=trainData)
    pred <- c(pred, predict(mod, newdata = testData, allow.new.levels = TRUE))
  }
  
  # Toon resultaten
  show_results_verschil(df, pred, categorie, eigen_gemeente)
  
  # hertrain op alle data
  mod <- glm(formula=formula, data=df)
  print(summary(mod))
  
  # sla het model op
  saveRDS(
    list(
      model_sig=stripGlmLR(mod),
      summary=capture.output(summary(mod)),
      formula=as.character(formula),
      regionaalniveau=regionaalniveau
    ), 
    file=file.path(
      model_dir, 
      sprintf("model_VNG_%s_%s.rds", categorie, regionaalniveau)
    )
  )
  
  return(
    list(
      model_sig=mod, 
      selected_variables=sub("diff_", "", intersect(all.vars(formula)[-1], names(df)))
    )
  )
}


# dit model is het eenvoudig 'first-differences' model uit Den Haag
haags_model_simpel <- function(df, categorie, regionaalniveau, eigen_gemeente = "Amsterdam") {
  print(sprintf("Grootte dataset voor model: %d", nrow(df)))
  # Onderstaand zijn de features die uit hun selectie zijn gekomen als meest belangrijk
  # Deze manier is op dit moment nog overbodig, maar er komt een moment waarop elke categorie zijn eigen voorspellende variabelen krijgt
  variables <- case_when(
    categorie %like% "wmo_hd" ~ c("aandeel_65Plus"),
    categorie %like% "wmo_oh" ~ c("aandeel_65Plus"),
    categorie %like% "wmo_hh" ~ c("aandeel_65Plus", "aandeel_ao", "aandeel_huishoudensZonderKinderen"),
    categorie %like% "totaal" ~ c("aandeel_65Plus","aandeel_gehuwd","aandeel_gescheiden"),
    TRUE ~ c("aandeel_65Plus")
  )
  # De case_when retourneerd altijd een vector van gelijke lengte,
  # dus in dit geval van lengte 3
  # Dat is erg onhandig.
  # voor nu repareren we dat door de unique waarden te nemen
  variables <- unique(variables)
  
  diff_variables <- paste("diff", variables, sep = "_")
  
  # Stel formule op met zonder intercept
  formula <- as.formula(
    paste0("diff_", categorie, ' ~ ', paste(diff_variables, collapse = ' + '), ' -1')
  )
  print(formula)
  
  # filter op het juiste regionale niveau
  df <- filter(df, regionaalniveau==!!regionaalniveau)
  
  # verwijder alle data met na
  df <- df[complete.cases(df[,intersect(all.names(formula), names(df))]),]
  
  print('model wordt getraind op:')
  print_aantal_wijken(df)
  
  # cross-validatie
  # verdeel de dataset in 10 gelijke subsets
  folds <- cut(seq(1, nrow(df)), breaks=10, labels=FALSE)
  
  # Voer 10-voudige cross-validatie uit
  pred <- c()
  for(i in 1:10){
    #Segmenteer de data met de folds gebruik makend van de which() functie
    testIndexes <- which(folds==i, arr.ind=TRUE)
    testData <- df[testIndexes, ]
    trainData <- df[-testIndexes, ]
    mod <- glm(formula=formula, data=trainData)
    pred <- c(pred, predict(mod, newdata = testData, allow.new.levels = TRUE))
  }
  
  # Toon resultaten
  show_results_verschil(df, pred, categorie, eigen_gemeente)
  
  # hertrain op alle data
  mod <- glm(formula=formula, data=df)
  print(summary(mod))
  
  # sla het model op
  saveRDS(
    list(
      model_sig=mod,
      summary=summary(mod),
      formula=formula
      ), 
    file=file.path(
      model_dir, 
      sprintf("model_den_haag_%s_%s.rds", categorie, regionaalniveau)
    )
  )
  
  return(list(model_sig=mod, selected_variables=variables))
}


show_results_verschil <- function(df_res, pred_res, categorie, eigen_gemeente){
  "
  Deze functie laat de prestatie van het verschil model zien
  "
  diff_categorie = paste0("diff_", categorie)
  
  print("=============================")
  print("Resultaten voor het verschil:")
  # show results
  plot(x=df_res[, diff_categorie], 
       y=pred_res, 
       col="blue",
       xlab="daadwerkelijk verschil",
       ylab="voorspeld verschil",
       main="Predictie per wijk voor verschillen"
  )
  abline(0, 1, lty="dashed", col="black")
  
  # Mape per wijk
  print("Per wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res, df_res[,diff_categorie])))
  
  # Mape per Amsterdamse wijk
  ind <- df_res$gemeentenaam == "Amsterdam"
  print("Per Amsterdamse wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], df_res[ind, diff_categorie])))
  
  # Mape per Haagse wijk
  ind <- df_res$gemeentenaam == "'s-Gravenhage"
  print("Per Haagse wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], df_res[ind, diff_categorie])))
  
  if(exists("eigen_gemeente")){
    # Mape per wijk voor je eigen gemeente
    ind <- df_res$gemeentenaam == eigen_gemeente
    print(paste0("Per wijk in ", eigen_gemeente, ":"))
    print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], df_res[ind, diff_categorie])))
    
    print("=============================")
    print("Resultaten voor aantallen:")
  }
  
  # reken de aantallen uit
  # Het is belangrijk om een 'round' uit te voeren op de actuals. 
  # Vanwege de grenswijzigingen kunnen sommige wmo-percentages erg laag worden
  # ingeschat. Dit kan dan Wmo-aantallen geven <1. Dergelijke lage Wmo-aantallen
  # kunnen hele hoge MAPE geven waardoor de gemiddelde MAPE op gemeenteniveau
  # onevenredig sterk wordt beinvloed. Dit kan hele hoge MAPE geven.
  pred_res <- (pred_res + df_res[,paste0(categorie, "_vorig_jaar")]) * df_res[,"aantal_inwoners"] / 100
  actuals <- round(df_res[,paste0(categorie, "_huidig_jaar")] * df_res[,"aantal_inwoners"] / 100)
  
  # show results
  plot(x=actuals, 
       y=pred_res, 
       col="blue",
       xlab="daadwerkelijke aantallen",
       ylab="voorspelde aantallen",
       main="Predictie per wijk voor aantallen"
  )
  abline(0, 1, lty="dashed", col="black")
  
  # Mape per wijk
  print("Per wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res, actuals)))
  
  # Mape per Amsterdamse wijk
  ind <- df_res$gemeentenaam == "Amsterdam"
  print("Per Amsterdamse wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], actuals[ind])))
  
  # Mape per Haagse wijk
  ind <- df_res$gemeentenaam == "'s-Gravenhage"
  print("Per Haagse wijk:")
  print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], actuals[ind])))

  if(exists("eigen_gemeente")){  
    # Mape per wijk voor je eigen gemeente
    ind <- df_res$gemeentenaam == eigen_gemeente
    print(paste0("Per wijk in ", eigen_gemeente, ":"))
    print(sprintf("MAPE: %f", mape.no.na(pred_res[ind], actuals[ind]))) 
  }  
  # Mape per gemeente
  temp <- cbind(df_res, voorspelling = pred_res, actual = actuals)
  aantallen_per_gemeente <- aggregate(
    cbind(actual, voorspelling) ~ gemeentenaam, 
    temp, 
    sum
  )
  
  print("Per gemeente:")
  print(sprintf("MAPE: %f", mape.no.na(aantallen_per_gemeente[,"voorspelling"], aantallen_per_gemeente[,"actual"])))
  
  
  # write to file
  con <- file(file.path(rapportage_dir, "output_mape"), "a")
  writeLines(format(Sys.time(), "Tijd: %X - Datum: %a %d %b %Y"), con)
  writeLines("---------------------------------------", con)
  writeLines(sprintf("Voorziening: %s", categorie), con)
  writeLines("Per wijk:", con)
  writeLines(sprintf("MAPE: %f", mape.no.na(pred_res, actuals)), con)
  writeLines("Per gemeente:", con)
  writeLines(
    sprintf(
      "MAPE: %f", 
      mape.no.na(
        aantallen_per_gemeente[,"voorspelling"], 
        aantallen_per_gemeente[,"actual"]
      )
    ),
    con
  )
  writeLines("\n", con)
  close(con)
  
}



stripGlmLR = function(cm) {
  "
  Functie om glm-modellen aanzienlijk kleiner te maken voor efficiente opslag.
  Gebasseerd op:
  https://win-vector.com/2014/05/30/trimming-the-fat-from-glm-models-in-r/
  "
  cm$y = c()
  cm$model = c()
  
  cm$residuals = c()
  cm$fitted.values = c()
  cm$effects = c()
  cm$qr$qr = c()  
  cm$linear.predictors = c()
  cm$weights = c()
  cm$prior.weights = c()
  cm$data = c()
  
  
  cm$family$variance = c()
  cm$family$dev.resids = c()
  cm$family$aic = c()
  cm$family$validmu = c()
  cm$family$simulate = c()
  attr(cm$terms,".Environment") = c()
  attr(cm$formula,".Environment") = c()
  
  cm
}
