# \# Exploreer modellen

In `exploreer_nieuwe_modellen.R` staan een aantal helperfuncties die kunnen helpen bij het exploreren van nieuwe modellen.

Het draaien van de gehele broncode van bovengenoemde file zorgt ervoor dat data kan worden ingeladen (`haal_train_data_op()`) en packages worden ingeladen etc. Daarmee kan dit script los van de rest van de code gedraaid worden.

## forward_feature_selection()

Deze functie past forward feature selection toe op de data. Daarbij wordt afgeweken van de standaard wijze van forward feature selection; er kunnen moderatoren worden toegevoegd én er kunnen meerdere modellen terugkomen. Daarover later meer. De aanroep van de functie is als volgt:

```         
f <- forward_feature_selection("mijn_uitkomst", c("feature1", "feature2", "feature3"), moderators = c("feature1", "feature4"), metric = "MAPE", max_depth = 7, regionaal_niveau = "gemeente", df = haal_train_data_op(), n_start_features=7)
```

De ondersteunde metrics zijn MAPE, RMSE, AIC en BIC. De max_depth geeft aan hoeveel features het model mag hebben.

De moteratorenlijst geeft aan of verkend moet worden of de features uit de featurelijst interacteren met de lijst moderators.

De n_start_features geeft aan hoeveel startmodellen gemaakt moeten worden. Deze forward feature selection function geeft dat aantal modellen terug. Dat doet de functie door in de eerste ronde van de forward feature selection de X sterkste univariate modellen te selecteren. Vervolgens wordt op deze X modellen de rest van de forward feature selection gedaan. De reden hiervoor is dat het mogelijk is dat de sterkste univariate relatie niet tot het beste model leidt, maar dat een andere combinatie daartoe leidt. Om dat enigszins (niet helemaal) te voorkomen is voor deze aanpak gekozen.

## get_mape()

Deze functie roept de MAPE functie aan zoals deze ook in de rest van de code wordt gebruikt. Deze functie verwacht de volledige formule als string en geeft de MAPEs terug.

```
get_mape("uitkomst ~ feature1 + feature2 + feature3")
```

## get_model_summary()

Deze functie verwacht de string als formule en het niveau waarop deze wordt toegepast ("gemeente" of "wijk"). Vervolgens komt de model summary stap voor stap terug, zodat duidelijk is hoe het model wordt opgebouwd. Dus bij een formule "uitkomst \~ feature1 + feature2 + feature3" komt eerst de model summary van "uitkomst \~ feature1", dan "uitkomst \~ feature1 + feature2" en dan "uitkomst \~ feature1 + feature2 + feature3".

```         
get_model_summary("uitkomst ~ feature1 + feature2 + feature3", "gemeente")
```

### get_correlated_vars_from_formula()

Geeft de univariate gecorreleerde variabelen terug. Daarbij kan de formule worden ingevoerd, en het regionaalniveau:

```         
get_correlated_vars_from_formula("uitkomst ~ feature1 + feature2 + feature3", "gemeente")
```

### get_pair_plot()

Geeft een pairplot terug. Hierbij kun je de outcome invoeren, een lijst features en het niveau.

```         
get_pair_plot(outcome, feature_list, regionaalniveau="gemeente")
```
