  library(dplyr)
  
  source("../../src/data/DataPreparatie.R")
  
  test_that("imputeer aandelen basic works", {
    
    #input data maken
    data <- tribble(
      ~jaar, ~gemeentecode, ~wijkcode, ~totaal_geneesmiddelen,
      2018, 'A','A',5,
      2018, 'A','B',NA,
      2020, 'C','D',NA,
      2020, 'C','C',6
      
    )
    imputeer_kolommen <- "totaal_geneesmiddelen"
    
    #output data maken
    expected <- tribble(
      ~jaar, ~gemeentecode, ~wijkcode, ~totaal_geneesmiddelen,
      2018, 'A','A',5,
      2018, 'A','B',5,
      2020, 'C','D',6,
      2020, 'C','C',6
    )
    
    #functie uitvoeren
    actual <- imputeer_aandelen(data = data ,imputeer_kolommen = imputeer_kolommen)
    
    #testen dat output van functie zelfde is als verwacht
    expect_true(
      all_equal(actual, expected)
    )
  })
   
  