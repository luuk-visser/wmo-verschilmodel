# Deployment op Azure (d.m.v. Docker)

Deze readme beschrijft hoe je het dashboard op Azure kunt zetten. Dit bestaat uit meerdere onderdelen. 
1. De initiële opzet van Docker en Azure
2. De update van Docker en Azure
3. Beheren van toegang en kosten op Azure

## Initiële deployment

Voor initiële deployment van het dashboard op Azure hebben we [dit stappenplan](https://towardsdatascience.com/deploying-dockerized-r-shiny-apps-on-microsoft-azure-the-quick-way-b554ec6c6e2f) gevolgd. 
De repository bevat al een Dockerfile die klaar is om een container mee op te zetten. 

1. Installeer [Docker](https://www.docker.com/products/docker-desktop) 
2. Installeer [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest&tabs=azure-cli).
3. Begin met de CLI (type `CMD` in de windows search bar) in de map van het dashboard
4. Inregelen van de Azure omgeving. 
    - Log in bij `az login` met de eigen credentials via de geopende webpagina
    - Maak een resource groep aan 
    
    `az group create --name wmo_dashboard_resources --location westeurope`

    - Maak een azure container registry, dit is nodig om de docker container in te plaatsen. 
    
    `az acr create -n wmoregistry -g wmo_dashboard_resources --sku Basic --admin-enabled true`

    - Maak een appservice plan aan. Dit is nodig voor de kostenstructuur van deze resource groep. 
    
    `az appservice plan create -g wmo_dashboard_resources -n wmodashboardplan --sku FREE --is-linux`

5. Opzetten van de docker container
    - Draai de commando's hieronder vanuit de `dashboard` folder.
    - Bouw de container met `docker build -t dashboard_wmo .`. Hiervoor wordt het image uit de repository gebruikt. De eerste keer dat de container gebouwd wordt kost dat circa 12 minuten.
    - Nadat de container gebouwd is, kun je deze lokaal draaien. Dat kun je doen via het commando `docker run -dp 3838:3838 dashboard_wmo`. Je kunt de container ook starten via de docker interface. Zorg ervoor dat je de poort op 3838 zet. Daarna is de dashboard toegankelijk via [http://localhost:3838](http://localhost:3838). 
    - Log vanuit Docker in bij de Azure container registry `docker login wmoregistry.azurecr.io`. Gebruik bij de gebruikersnaam en wachtwoord die te zien zijn onder de 'Access keys'-pagina van de 'Container registries' op Azure. Hiervoor moet de switch van 'Admin user' op 'Enabled' staan (als het goed is, is dit al het geval).
    - Geef de container een tag om deze goed te kunnen herkennen `docker tag wmoregistry.azurecr.io/dashboard_wmo`.
    - Push de gemaakte container naar Azure container registry `docker push wmoregistry.azurecr.io/dashboard_wmo`.
6. Aanmaken en inregelen van de Azure webapp (hiervoor moet er al een docker container aanwezig zijn)
    - Aanmaken van de webapp die de container beschikbaar maakt voor de buitenwereld 
    
    `az webapp create -g wmo_dashboard_resources -p wmodashboardplan -n wmo-voorspelmodel -i wmoregistry.azurecr.io/dashboard_wmo`
    
    - Inregelen van de webapp, zoals het goed zetten van de poort 
    
    `az webapp config appsettings set --resource-group wmo_dashboard_resources --name wmo-voorspelmodel --settings WEBSITES_PORT=3838`

Hierna is het dashboard beschikbaar op [https://wmo-voorspelmodel.azurewebsites.net](https://wmo-voorspelmodel.azurewebsites.net)

## Updaten van het dashboard

Om een nieuwe versie van het dashboard te deployen op Azure moeten de volgende commando's gerund worden via de CLI. Ga hiervoor opnieuw eerst in de CLI naar de map waarin alle bestanden voor het dashboard te vinden zijn.

1. Installeer (indien nodig) [Docker](https://www.docker.com/products/docker-desktop) 
2. Installeer (indien nodig) [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-windows?view=azure-cli-latest&tabs=azure-cli).
3. Begin met de CLI (type `CMD` in de windows search bar) in de map van het dashboard
4. Opzetten van de docker container.
    - Log in bij `az login` met de eigen credentials via de geopende webpagina
    - Bouw de container met `docker build -t dashboard_wmo .`. Hiervoor wordt het image uit de repository gebruikt. 
    - Nadat de container gebouwd is, kun je deze lokaal draaien. Dat kun je doen via het commando `docker run -dp 3838:3838 dashboard_wmo`. Je kunt de container ook starten via de docker interface. Zorg ervoor dat je de poort op 3838 zet. Daarna is de dashboard toegankelijk via [http://localhost:3838](http://localhost:3838). 
    - Geef de container een tag om deze goed te kunnen herkennen `docker tag dashboard_wmo wmoregistry.azurecr.io/dashboard_wmo`
    - Log vanuit Docker in bij de Azure container registry `docker login wmoregistry.azurecr.io`. Gebruik bij de gebruikersnaam en wachtwoord die te zien zijn onder de 'Access keys'-pagina van de 'Container registries' op Azure. Hiervoor moet de switch van 'Admin user' op 'Enabled' staan (als het goed is, is dit al het geval).
    - Push de gemaakte container naar Azure container registry `docker push wmoregistry.azurecr.io/dashboard_wmo` 
5. Vernieuwen van de webapp
    
    - Door dit commando wordt de webapp vernieuwd. `az webapp create -g wmo_dashboard_resources -p wmodashboardplan -n wmo-voorspelmodel -i wmoregistry.azurecr.io/dashboard_wmo`
    - Mogelijk werkt dit commando niet, parallel of alternatief is de webapp herstarten vanuit de Azure webportal. Het duurt dan een minuutje voordat het vernieuwde dashboard dan beschikbaar is.
    
 


Logs zijn te vinden onder de opgezette App Service (op de website van Azure) onder Advanced Tools.


## Beheer van Azure

### Beperkte toegang

In de Azure portal kun je zien wie er toegang heeft tot het dashboard. Dat staat op [wmo-voorspelmodel | Networking](https://portal.azure.com/#@WMOVoorspelmodel.onmicrosoft.com/resource/subscriptions/4d9f9d66-4002-4122-abe4-d4219e624766/resourceGroups/wmo_dashboard_resources/providers/Microsoft.Web/sites/wmo-voorspelmodel/networkingHub). Je kunt de toegang ook via de command line regelen. Die methoden staan hieronder.

Voor het verlenen van toegang aan een selecte groep is ip-whitelisting het meest logisch (voor een kleine groep). Dit gebeurt met volgend commando, waarbij de ip-adressen ipv4-adressen zijn, gescheiden met komma's:
```
az webapp config access-restriction add --resource-group wmo_dashboard_resources --name wmo-voorspelmodel --rule-name 'Wmoteam' --action Allow --ip-address "<ip-adressen>" --priority 100
```

Indien iedereen opnieuw toegang mag hebben kan dat worden uitgevoerd als (hiermee wordt de regel verwijderd):
```
az webapp config access-restriction remove --resource-group wmo_dashboard_resources --name wmo-voorspelmodel --rule-name 'Wmoteam'
```

### Custom domain

De originele website is wmo-voorspelmodel.azurewebsites.net. Dit is niet de naam die we willen, dus is aan I&A gevraagd voor een CNAME-record: wmovoorspelmodel.vng.nl. Vervolgens is een SNI-SSL certificaat hieraan gebonden via [dit Azure stappenplan](https://docs.microsoft.com/en-us/azure/app-service/configure-ssl-bindings), zodat er geen veiligheidsissues voorkomen bij het openen van de website.

### Kosten

De kosten van deze resource groep kunnen worden gemonitord via [wmo_dashboard_resources | Cost analysis](https://portal.azure.com/#@WMOVoorspelmodel.onmicrosoft.com/resource/subscriptions/4d9f9d66-4002-4122-abe4-d4219e624766/resourcegroups/wmo_dashboard_resources/costanalysis)
